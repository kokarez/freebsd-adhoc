#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

from scapy.all import *

#####################################################
#        Define fashoc packet for scapy
#####################################################

class Fadhoc(Packet):

  name = "Fadhoc"
  fields_desc = [
      MACField("rdst", ETHER_ANY),
      MACField("rsrc", ETHER_ANY),
      XShortEnumField("rtype", 0x0000, ETHER_TYPES),
      IntField("selector", 0),
  ];

bind_layers(Ether,  Fadhoc, type=0x1664)
bind_layers(Fadhoc, IP,     rtype=2048)

#####################################################
#            Main code, forge and send
#####################################################

fadhoc = Fadhoc()
fadhoc.rdst = "00:0f:fe:e8:40:4b"
pack = Ether(dst="00:0f:fe:e8:40:4b", src="00:21:cc:db:69:10") / fadhoc  / IP() / ICMP()
pack.show()
sendp(pack)
