#include <sys/param.h>
#include <sys/timeet.h>

#include "src/fadhoc.h"
#include "src/timer.h"

struct callout clean_route_timer;

void timer_do_callback(timeout_t *callback, void *arg, int ticks) {
  timeout(callback, arg, ticks);
}

static void timer_callback(void *data) {
  
  if (fadhoc_route_is_ready())
    fadhoc_route_clean(data);
  callout_reset(&clean_route_timer, FADHOC_TIMER_TICKS, timer_callback, data);
}

int timer_init() {

  callout_init(&clean_route_timer, 1);
  callout_stop(&clean_route_timer);
  callout_reset(&clean_route_timer, FADHOC_TIMER_TICKS, timer_callback, NULL);
  callout_schedule(&clean_route_timer, FADHOC_TIMER_TICKS); 

  return 0;
}

int timer_fini() {
  callout_stop(&clean_route_timer);
  return 0;
}
