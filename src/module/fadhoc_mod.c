#include <sys/param.h>
#include <sys/module.h>
#include <sys/kernel.h>
#include <sys/syslog.h>
#include <sys/systm.h>

#include "src/fadhoc.h"
#include "src/netbox.h"
#include "src/timer.h"

static int fadhoc_load(struct module *module, void *arg) {
  int err = 0;

  dprintf("Fadhoc load\n");
  if ((err = netbox_init()))
    goto error;

  if ((err = timer_init()))
    goto error;

error:
  return err;
}

static int fadhoc_unload(struct module *module, void *arg) {
  int err = 0;
  
  dprintf("Fadhoc unload\n");
  err = netbox_fini();
  err = timer_fini();
  return err;
}

static int fadhoc_event_hdl(struct module *module, int event, void *arg) {

  int err = 0;
  
  switch (event) {
    case MOD_LOAD:
      err = fadhoc_load(module, arg);
      break;

    case MOD_UNLOAD:
      err = fadhoc_unload(module, arg);
      break;

    default:
      dprintf("Unkown event %d\n", event);
      err = EOPNOTSUPP;
      break;
  }

  return err;
}

static moduledata_t fadhoc_conf = {
  "fadhoc",
  fadhoc_event_hdl,
  NULL
};

DECLARE_MODULE(fadhoc, fadhoc_conf, SI_SUB_DRIVERS, SI_ORDER_MIDDLE);
