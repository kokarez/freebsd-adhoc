#include <sys/param.h>
#include <sys/module.h>
#include <sys/kernel.h>
#include <sys/systm.h>
#include <sys/syslog.h>
#include <sys/socket.h>

#include <net/if.h>
#include <net/if_dl.h>
#include <net/if_var.h>
#include <net/if_types.h>
#include <net/bpf.h>
#include <net/ethernet.h>

#include "src/fadhoc.h"
#include "src/netbox.h"

static netbox_if_input hard_if_input = NULL;
static netbox_if_input hook_if_input = NULL;
static struct ifnet    *hard_ifnet = NULL;

struct ifnet *netbox_hard_get() {
  return hard_ifnet;
}

/*!
 * @brief This function redirect if_input if packet is type ETHERTYPE_FADHOC
 * @param ifnet interface
 * @param m mbuf packet receive from network
*/
static void netbox_if_input_schedule(struct ifnet *ifnet, struct mbuf *m) {
  struct ether_header *eh;
  u_short             etype;
 
  eh = mtod(m, struct ether_header *);
  etype = ntohs(eh->ether_type);

  if (hard_if_input && etype != ETHERTYPE_FADHOC) {
    hard_if_input(ifnet, m);
    return;
  }

  if (hook_if_input && etype == ETHERTYPE_FADHOC) {
    hook_if_input(ifnet, m);
    return;
  }

  dprintf("Reach code unreachable: hook:%p hard:%p etype:0x%x\n",
          hook_if_input,
          hard_if_input,
          etype);
}

/*!
 * @brief this function add an filter on the hard interface to get needed packet
*/
int netbox_hard_init() {
  char *ifname = fadhoc_getenv(FADHOC_HINT_HARD_IFNET_NAME);

  if (!ifname) {
    duprintf("\""
             FADHOC_HINT
             FADHOC_HINT_HARD_IFNET_NAME
             "\" not set in kenv(9)\n");
    return EINVAL;
  }

  hard_ifnet = ifunit(ifname);
  if (!hard_ifnet) {
    duprintf("Interface %s doesn't exist\n", ifname);
    return ENODEV;
  }

  hard_if_input = hard_ifnet->if_input;
  hook_if_input = netbox_recv_from_ether;
  hard_ifnet->if_input = netbox_if_input_schedule;

  return 0;
}

int netbox_hard_fini() {
  if (hard_ifnet && hard_if_input)
    hard_ifnet->if_input = hard_if_input; 
  return 0;
}
