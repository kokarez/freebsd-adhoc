#include <sys/param.h>
#include <sys/module.h>
#include <sys/kernel.h>
#include <sys/systm.h>
#include <sys/syslog.h>
#include <sys/socket.h>

#include <net/if.h>
#include <net/if_dl.h>
#include <net/if_var.h>
#include <net/if_types.h>
#include <net/bpf.h>

#include "src/fadhoc.h"
#include "src/netbox.h"


static const u_char etherbroadcastaddr[ETHER_ADDR_LEN] =
			{ 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };

#define	ETHER_IS_BROADCAST(addr)                          \
	(bcmp(etherbroadcastaddr, (addr), ETHER_ADDR_LEN) == 0)

/*!
 * @bief this function encapsule the packet to send it to the ether
 * @param packet send to the ether
*/
void netbox_send_to_ether(struct fadhoc_packet *fpkt, unsigned char *mac_dst) {
  struct mbuf           *m = fpkt->pkt;
  struct fadhoc_header  *fh;
  struct ether_header   *eh;

  if (!netbox_hard_get())
    return;

  /*
   * Allocate space for fadhoc header
   */
  M_PREPEND(m, sizeof (struct fadhoc_header), M_NOWAIT);

  fh = mtod(m, struct fadhoc_header *);
  bcopy(fpkt, fh, ETHER_ADDR_LEN * 2);
  fh->type = htons(fpkt->type);
  fh->selector = htonl(fpkt->selector);

  /*
   * Allocate space for ethernet header
   */
  M_PREPEND(m, ETHER_HDR_LEN, M_NOWAIT);

  eh = mtod(m, struct ether_header *);
  eh->ether_type = htons(ETHERTYPE_FADHOC);
  bcopy(mac_dst, eh->ether_dhost, ETHER_ADDR_LEN);
  bcopy(IF_LLADDR(netbox_hard_get()), eh->ether_shost, ETHER_ADDR_LEN);

  dprintf("mac_dst: %s\n", fad_eth2string(mac_dst));

  netbox_hard_get()->if_transmit(netbox_hard_get(), m);
}

/*!
 * @brief do a fake l2 encaptualation and send it on the satck
 * @param packet send to the stack
*/
void netbox_send_to_stack(struct fadhoc_packet *fpkt) {

}

/*!
 * @brief packet of type fadhoc receive from ethernet
 * @param ifp hard interface
 * @param m packet
 * At then end of this function the mbuf chain is free because we
 * think that no data were stock at the ether recv
 */
void netbox_recv_from_ether(struct ifnet *ifp, struct mbuf *m) {
  struct ether_header   *eh;
  struct m_tag          *mtag;
  struct fadhoc_packet  *fpkt, *fh;
  unsigned char         src[6];

  eh = mtod(m, struct ether_header *);
  dprintf("ifxname:%s dst:%s src:%s\n",
          ifp->if_xname,
          fad_eth2string(eh->ether_dhost),
          fad_eth2string(eh->ether_shost));
  bcopy(eh->ether_shost, src, ETHER_ADDR_LEN);

  /*
   * Remove ether header from data
   */
  m_adj(m, sizeof (struct ether_header));

  /*
   * Create private place in packet and copy fadhoc information in
   */
  mtag = m_tag_alloc(MTAG_FADHOC_COOKIES, MTAG_FADHOC_TYPE, sizeof (struct fadhoc_packet), M_NOWAIT);
  fpkt = (struct fadhoc_packet *)(mtag + 1);
  fh = mtod(m, struct fadhoc_packet *);
  memcpy(fpkt, fh, sizeof (struct fadhoc_packet));

  /*
   * Apply network big/little endian modification on fadhoc data
   */
  fpkt->type = ntohs(fh->type);

  /*
   * Remove fadhoc header from data
   */
  m_adj(m, sizeof (struct fadhoc_packet));

  fpkt->pkt = m;
  fpkt->dst_is_me = !memcmp(IF_LLADDR(netbox_hard_get()), fpkt->dhost, ETHER_ADDR_LEN);

  fadhoc_route_fpkt(fpkt);

  m_freem(m);
}

/*!
 * @brief packet receive from the kernel stack
 * @param ifp soft interface
 * @param m packet
 */
void netbox_recv_from_stack(struct ifnet *ifp, struct mbuf *m) {
  struct ether_header   *eh;
  struct m_tag          *mtag;
  struct fadhoc_packet  *fpkt;
  struct fadhoc_header  *fh;

  if (m->m_len < sizeof (struct ether_header)) {
    dprintf("packet with bad size (%d)\n", m->m_len);
    return;
  }

  if (m->m_len > ETHERMTU_FADHOC) {
    dprintf("MTU is not respected, packet can't be modifed mtu_expeted:%lu mtu_found:%d\n",
            ETHERMTU_FADHOC,
            m->m_len);
    return;
  }

  eh = mtod(m, struct ether_header *);
  dprintf("ifxname:%s dst:%s src:%s type:%d\n",
          ifp->if_xname,
          fad_eth2string(eh->ether_dhost),
          fad_eth2string(eh->ether_shost),
          htons(eh->ether_type));

  mtag = m_tag_alloc(MTAG_FADHOC_COOKIES, MTAG_FADHOC_TYPE, sizeof (struct fadhoc_packet), M_NOWAIT);
  fpkt = (struct fadhoc_packet *)(mtag + 1);
  fh = mtod(m, struct fadhoc_header *);
  memcpy(fpkt, eh, ETHER_ADDR_LEN * 2);

  fpkt->type = ntohs(fpkt->type);
  fpkt->selector = 0x1234; /* TODO: do real rootage */

  /*
   * Remove ether header from data
   */
  m_adj(m, sizeof (struct ether_header));

  fpkt->pkt = m;
  fpkt->dst_is_me = 0;
  
  fadhoc_route_fpkt(fpkt);
}

/*!
 * @brief this function init network interface flux
 * This function add an hook in the hard interface and
 * create an soft interface to get flux.
 *   hard -> netbox_recv_from_ether
 *   soft -> netbox_recv_from_stack
*/
int netbox_init() {
  int err = 0;

  if ((err = netbox_hard_init()))
    goto error;

  if ((err = netbox_soft_init()))
    goto error;

error:
  return err;
}

int netbox_fini() {
  int err = 0;
  netbox_hard_fini();
  netbox_soft_fini();
  return err;
}
