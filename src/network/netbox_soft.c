#include <sys/param.h>
#include <sys/module.h>
#include <sys/kernel.h>
#include <sys/systm.h>
#include <sys/syslog.h>
#include <sys/socket.h>

#include <net/if.h>
#include <net/if_var.h>
#include <net/if_types.h>
#include <net/bpf.h>

#include "src/fadhoc.h"
#include "src/netbox.h"

static struct ifnet       *g_soft_ifnet = NULL;

/*!
 * @brief this function is call when the interface is ask to be running by user
 * @param priv nothing
*/
static void netbox_soft_if_init(void *priv) {
  dprintf("set IFF_DRV_RUNNING\n");
  g_soft_ifnet->if_drv_flags |= IFF_DRV_RUNNING;
}

/*!
 * @brief this function is call when packet must be transmit
 * @param ifp soft interface
*/
static void netbox_soft_if_start(struct ifnet *ifp) {
  struct mbuf *m;

  if ((ifp->if_drv_flags & (IFF_DRV_RUNNING | IFF_DRV_OACTIVE)) != IFF_DRV_RUNNING)
    return;

  ifp->if_drv_flags |= IFF_DRV_OACTIVE;
  IFQ_LOCK(&ifp->if_snd);
  while (!IFQ_IS_EMPTY(&ifp->if_snd)) {
    ifp->if_opackets++;
    IFQ_DEQUEUE(&ifp->if_snd, m);
    if (m != NULL) {
      BPF_MTAP(ifp, m); 
      netbox_recv_from_stack(ifp, m);
      m_freem(m);
    }
  }
  IFQ_UNLOCK(&ifp->if_snd);
  ifp->if_drv_flags &= ~IFF_DRV_OACTIVE;
}

/*!
 * @brief This function setup the soft interface
 * 1 - get name of the soft interface in FADHOC_HINT_SOFT_IFNET_NAME
 * 2 - check if the interface exist
 * 3 - create interface
 *   a - ether type (with all ether function)
 *   b - set init function -> netbox_soft_if_init
 *   c - set start function -> netbox_soft_if_start
 *   d - attach interface and set mac
 *   e - change the mtu size (if_mtu - FADHOC_HDR_LEN)
*/
int netbox_soft_init() {
  int           err = 0;
  char          *ifname;
  struct ifnet  *ifsoft;

  ifname = fadhoc_getenv(FADHOC_HINT_SOFT_IFNET_NAME);
  if (!ifname) {
    duprintf("\""
             FADHOC_HINT
             FADHOC_HINT_SOFT_IFNET_NAME
             "\" not set in kenv(9)\n");
    return EINVAL; 
  }

  ifsoft = ifunit(ifname);
  if (ifsoft) {
    duprintf("Interface '%s' already exist, can't use it for soft if\n", ifname);
    return EINVAL;
  }

  ifsoft = if_alloc(IFT_ETHER);
  if (!ifsoft) {
    duprintf("Can't alloc ifnet for %s\n", ifname);
    return ENOMEM;
  }

  if_initname(ifsoft, ifname, IF_DUNIT_NONE); 
  
  ifsoft->if_flags = IFF_BROADCAST | IFF_SIMPLEX | IFF_MULTICAST;
  ifsoft->if_init = netbox_soft_if_init;
  ifsoft->if_start = netbox_soft_if_start;

  ifsoft->if_ioctl = ether_ioctl;

  IFQ_SET_MAXLEN(&ifsoft->if_snd, IFQ_MAXLEN);
  ifsoft->if_snd.ifq_drv_maxlen = 0;
  IFQ_SET_READY(&ifsoft->if_snd);
  
  ether_ifattach(ifsoft, "\xaa\xaa\xaa\xbb\xbb\xbb");
  ifsoft->if_baudrate = 0;
  g_soft_ifnet = ifsoft;

  ifsoft->if_mtu = ETHERMTU_FADHOC;

  return err;
}

int netbox_soft_fini(void) {
  int err = 0;

  if (g_soft_ifnet) {
    ether_ifdetach(g_soft_ifnet);
    g_soft_ifnet = NULL;
  }
  return err;
}
