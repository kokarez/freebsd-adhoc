#include "src/fadhoc.h"
#include "src/netbox.h"

int fadhoc_route_is_ready() {
  return 1;
}

void fadhoc_route_clean() {
  size_t rt_deleted = 0;

  if (rt_deleted)
    dprintf("clean %zu route\n", rt_deleted);
}

void fadhoc_route_fpkt(struct fadhoc_packet *fpkt) {
  fadhoc_dump_fpkt(fpkt);

  if (fpkt->dst_is_me)
    netbox_send_to_stack(fpkt);
  else
    netbox_send_to_ether(fpkt, "\xff\xff\xff\xff\xff\xff");
}
