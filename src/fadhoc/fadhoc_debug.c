#include "src/fadhoc.h"

void fadhoc_dump_fpkt(struct fadhoc_packet *fpkt) {
  dprintf("fpkt: src:%s dst:%s type:0x%x pkt:0x%p is_me:%d\n",
          fad_eth2string(fpkt->shost),
          fad_eth2string(fpkt->dhost),
          fpkt->type,
          fpkt->pkt,
          fpkt->dst_is_me);
}
