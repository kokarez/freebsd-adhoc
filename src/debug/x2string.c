#include "src/fadhoc.h"

# define DBUF_LEN 4096

# define STR_ETHER_LEN 18

static char debug_buffer[DBUF_LEN];
static unsigned int debug_index = 0;

static char *debug_getbuf(size_t size) {

  if (debug_index + size >= DBUF_LEN)
    debug_index = 0;
  else
    debug_index += size;

  return debug_buffer + debug_index;
}

char *fad_eth2string(const unsigned char *mac) {
  char *mac_tmp;
  
  mac_tmp = debug_getbuf(STR_ETHER_LEN * sizeof (char));

  snprintf(mac_tmp, STR_ETHER_LEN, "%02x:%02x:%02x:%02x:%02x:%02x",
           mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);

  return mac_tmp;
}
