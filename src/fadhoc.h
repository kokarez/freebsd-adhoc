#ifndef FADHOC_H
# define FADHOC_H

# include <sys/param.h>
# include <sys/module.h>
# include <sys/kernel.h>
# include <sys/syslog.h>
# include <sys/systm.h>

# define FADHOC_TAG "[FDH]"
# define FADHOC_HINT "hint.fadhoc.0."

# define FADHOC_HINT_HARD_IFNET_NAME "ifnet.hard.name"
# define FADHOC_HINT_SOFT_IFNET_NAME "ifnet.soft.name"
# define FADHOC_IFNET_DNAME "fadhoc"

# define fadhoc_getenv(keyword) getenv(FADHOC_HINT keyword)
# define dprintf(format, args...) log(LOG_DEBUG, FADHOC_TAG " %s: " format, __func__, ## args)
# define duprintf(format, args...) \
  uprintf(format, ## args);        \
  dprintf(format, ## args);

struct fadhoc_packet {
  unsigned char   dhost[6];
  unsigned char   shost[6];
  unsigned short  type;
  unsigned int    selector;
  void            *pkt;
  unsigned char   dst_is_me;
} __attribute__((packed));

void fadhoc_dump_fpkt(struct fadhoc_packet *fpkt);
char *fad_eth2string(const unsigned char *mac);

void fadhoc_route_clean(void *);
int  fadhoc_route_is_ready(void);
void fadhoc_route_fpkt(struct fadhoc_packet *fpkt);

int fadhoc_init(void);
int fadhoc_fini(void);

#endif /* !FADHOC_H */
