#ifndef TIMER_H
# define TIMER_H

# define FADHOC_TIMER_NAME "FADTIMER"
# define FADHOC_TIMER_QUALITY 800
# define FADHOC_TIMER_PERIOD_MIN (0x4000LLU) 
# define FADHOC_TIMER_PERIOD_MAX (0xfffffffeLLU << 32)
# define FADHOC_TIMER_TICKS       (1 << 10)

# define FADHOC_TIMER_ST_NONE     (0 << 0)
# define FADHOC_TIMER_ST_REGISTER (1 << 0)
# define FADHOC_TIMER_ST_RUNNING  (1 << 1)

# define FADHOC_TIMER_PERIOD       (200)
# define FADHOC_TIMER_PERIOD_START (200)

void timer_do_callback(timeout_t *callback, void *arg, int ticks);
int timer_init(void);
int timer_fini(void);

#endif /* !TIMER_H */
