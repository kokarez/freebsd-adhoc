#ifndef NETBOX_H
# define NETBOX_H

# include <sys/socket.h>
# include <sys/mbuf.h>

# include <net/if.h>
# include <net/if_var.h>
# include <net/if_types.h>
# include <net/ethernet.h>

# define ETHERTYPE_FADHOC 0x1664
# define ETHERMTU_FADHOC  (ETHERMTU - sizeof (struct fadhoc_packet))
# define MTAG_FADHOC_COOKIES 0x1664
# define MTAG_FADHOC_TYPE    0

typedef void (*netbox_if_input)(struct ifnet *ifp, struct mbuf *m);
typedef int (*netbox_if_ouput)(struct ifnet *ifp, struct mbuf *m, struct sockaddr *dst, struct route *rt);
typedef int (*netbox_if_transmit)(struct ifnet *ifp, struct mbuf *m);

# define FADHOC_HDR_LEN (sizeof (struct fadhoc_packet))

struct fadhoc_header {
  unsigned char   dhost[6];
  unsigned char   shost[6];
  unsigned short  type;
  unsigned int    selector; 
};

/*
 * Hard interface managment
*/

int netbox_hard_init(void);
int netbox_hard_fini(void);

/*
 * Soft interface managment
*/

int netbox_soft_init(void);
int netbox_soft_fini(void);

/*
 * general netbox init and fini function
*/

struct ifnet *netbox_hard_get(void);
void netbox_send_to_ether(struct fadhoc_packet *fpkt, unsigned char *mac);
void netbox_send_to_stack(struct fadhoc_packet *fpkt);
void netbox_recv_from_stack(struct ifnet *ifp, struct mbuf *m);
void netbox_recv_from_ether(struct ifnet *ifp, struct mbuf *m);
int netbox_init(void);
int netbox_fini(void);

#endif /* !NETBOX_H */
